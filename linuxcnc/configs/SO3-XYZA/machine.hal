loadrt [KINS]KINEMATICS
loadrt [EMCMOT]EMCMOT servo_period_nsec=[EMCMOT]SERVO_PERIOD num_joints=[KINS]JOINTS
loadrt hostmot2
loadrt hm2_eth board_ip="10.10.10.10" config=" num_encoders=0 num_pwmgens=0 num_stepgens=5 sserial_port_0=00xxxx" 
setp    hm2_7i76e.0.watchdog.timeout_ns 5000000
loadrt pid names=pid.j0,pid.j1,pid.j2,pid.j3,pid.j4,pid.s

addf hm2_7i76e.0.read                   servo-thread
addf motion-command-handler             servo-thread
addf motion-controller                  servo-thread
addf pid.j0.do-pid-calcs                servo-thread
addf pid.j1.do-pid-calcs                servo-thread
addf pid.j2.do-pid-calcs                servo-thread
addf pid.j3.do-pid-calcs                servo-thread
addf pid.j4.do-pid-calcs                servo-thread
addf pid.s.do-pid-calcs                 servo-thread
addf hm2_7i76e.0.write                  servo-thread
setp hm2_7i76e.0.dpll.01.timer-us       -50
setp hm2_7i76e.0.stepgen.timer-number   1

# --- HOME SWITCHES ---
net home-0       <=  hm2_7i76e.0.7i76.0.0.input-06-not
net home-1-2     <=  hm2_7i76e.0.7i76.0.0.input-04-not
net home-3       <=  hm2_7i76e.0.7i76.0.0.input-07-not
net home-4       <=  hm2_7i76e.0.7i76.0.0.input-05-not
net probe-in     <=  hm2_7i76e.0.7i76.0.0.input-08-not

net home-0       =>  joint.0.home-sw-in
net home-1-2     =>  joint.1.home-sw-in
net home-1-2     =>  joint.2.home-sw-in
net home-3       =>  joint.3.home-sw-in
net home-4       =>  joint.4.home-sw-in

#*******************
#  JOINT 0
#*******************
setp   pid.j0.Pgain     [JOINT_0]P
setp   pid.j0.Igain     [JOINT_0]I
setp   pid.j0.Dgain     [JOINT_0]D
setp   pid.j0.bias      [JOINT_0]BIAS
setp   pid.j0.FF0       [JOINT_0]FF0
setp   pid.j0.FF1       [JOINT_0]FF1
setp   pid.j0.FF2       [JOINT_0]FF2
setp   pid.j0.deadband  [JOINT_0]DEADBAND
setp   pid.j0.maxoutput [JOINT_0]MAX_OUTPUT
setp   pid.j0.error-previous-target true
setp   pid.j0.maxerror 0.012700

net j0-index-enable  <=> pid.j0.index-enable
net j0-enable        =>  pid.j0.enable
net j0-pos-cmd       =>  pid.j0.command
net j0-pos-fb        =>  pid.j0.feedback
net j0-output        <=  pid.j0.output

net j0-pos-cmd    <= joint.0.motor-pos-cmd
net j0-vel-cmd    <= joint.0.vel-cmd
net j0-pos-fb     => joint.0.motor-pos-fb
net j0-enable     <= joint.0.amp-enable-out

# Step Gen signals/setup

setp   hm2_7i76e.0.stepgen.00.dirsetup        [JOINT_0]DIRSETUP
setp   hm2_7i76e.0.stepgen.00.dirhold         [JOINT_0]DIRHOLD
setp   hm2_7i76e.0.stepgen.00.steplen         [JOINT_0]STEPLEN
setp   hm2_7i76e.0.stepgen.00.stepspace       [JOINT_0]STEPSPACE
setp   hm2_7i76e.0.stepgen.00.position-scale  [JOINT_0]STEP_SCALE
setp   hm2_7i76e.0.stepgen.00.step_type        0
setp   hm2_7i76e.0.stepgen.00.control-type     1
setp   hm2_7i76e.0.stepgen.00.maxaccel         [JOINT_0]STEPGEN_MAXACCEL
setp   hm2_7i76e.0.stepgen.00.maxvel           [JOINT_0]STEPGEN_MAXVEL

net j0-output     <= hm2_7i76e.0.stepgen.00.velocity-cmd
net j0-pos-fb     <= hm2_7i76e.0.stepgen.00.position-fb
net j0-enable     => hm2_7i76e.0.stepgen.00.enable

#*******************
# JOINT 1
#*******************

setp   pid.j1.Pgain     [JOINT_1]P
setp   pid.j1.Igain     [JOINT_1]I
setp   pid.j1.Dgain     [JOINT_1]D
setp   pid.j1.bias      [JOINT_1]BIAS
setp   pid.j1.FF0       [JOINT_1]FF0
setp   pid.j1.FF1       [JOINT_1]FF1
setp   pid.j1.FF2       [JOINT_1]FF2
setp   pid.j1.deadband  [JOINT_1]DEADBAND
setp   pid.j1.maxoutput [JOINT_1]MAX_OUTPUT
setp   pid.j1.error-previous-target true
# This setting is to limit bogus stepgen
# velocity corrections caused by position
# feedback sample time jitter.
setp   pid.j1.maxerror 0.012700

net j1-index-enable  <=> pid.j1.index-enable
net j1-enable        =>  pid.j1.enable
net j1-pos-cmd       =>  pid.j1.command
net j1-pos-fb        =>  pid.j1.feedback
net j1-output        <=  pid.j1.output

# Step Gen signals/setup

setp   hm2_7i76e.0.stepgen.01.dirsetup        [JOINT_1]DIRSETUP
setp   hm2_7i76e.0.stepgen.01.dirhold         [JOINT_1]DIRHOLD
setp   hm2_7i76e.0.stepgen.01.steplen         [JOINT_1]STEPLEN
setp   hm2_7i76e.0.stepgen.01.stepspace       [JOINT_1]STEPSPACE
setp   hm2_7i76e.0.stepgen.01.position-scale  [JOINT_1]STEP_SCALE
setp   hm2_7i76e.0.stepgen.01.step_type        0
setp   hm2_7i76e.0.stepgen.01.control-type     1
setp   hm2_7i76e.0.stepgen.01.maxaccel         [JOINT_1]STEPGEN_MAXACCEL
setp   hm2_7i76e.0.stepgen.01.maxvel           [JOINT_1]STEPGEN_MAXVEL

# ---closedloop stepper signals---

net j1-output     <= hm2_7i76e.0.stepgen.01.velocity-cmd
net j1-pos-fb     <= hm2_7i76e.0.stepgen.01.position-fb
net j1-enable     => hm2_7i76e.0.stepgen.01.enable
net j1-pos-cmd    <= joint.1.motor-pos-cmd
net j1-vel-cmd    <= joint.1.vel-cmd
net j1-pos-fb     => joint.1.motor-pos-fb
net j1-enable     <= joint.1.amp-enable-out


#*******************
#  JOINT 2 (Tandem to Joint 1)
#*******************

setp   pid.j2.Pgain     [JOINT_2]P
setp   pid.j2.Igain     [JOINT_2]I
setp   pid.j2.Dgain     [JOINT_2]D
setp   pid.j2.bias      [JOINT_2]BIAS
setp   pid.j2.FF0       [JOINT_2]FF0
setp   pid.j2.FF1       [JOINT_2]FF1
setp   pid.j2.FF2       [JOINT_2]FF2
setp   pid.j2.deadband  [JOINT_2]DEADBAND
setp   pid.j2.maxoutput [JOINT_2]MAX_OUTPUT
setp   pid.j2.error-previous-target true
# This setting is to limit bogus stepgen
# velocity corrections caused by position
# feedback sample time jitter.
setp   pid.j2.maxerror 0.012700

net j2-index-enable  <=> pid.j2.index-enable
net j2-enable        =>  pid.j2.enable
net j2-pos-cmd       =>  pid.j2.command
net j2-pos-fb        =>  pid.j2.feedback
net j2-output        <=  pid.j2.output



# Step Gen signals/setup for tandem axis

setp   hm2_7i76e.0.stepgen.02.dirsetup        [JOINT_2]DIRSETUP
setp   hm2_7i76e.0.stepgen.02.dirhold         [JOINT_2]DIRHOLD
setp   hm2_7i76e.0.stepgen.02.steplen         [JOINT_2]STEPLEN
setp   hm2_7i76e.0.stepgen.02.stepspace       [JOINT_2]STEPSPACE
setp   hm2_7i76e.0.stepgen.02.position-scale  [JOINT_2]STEP_SCALE
setp   hm2_7i76e.0.stepgen.02.step_type        0
setp   hm2_7i76e.0.stepgen.02.control-type     1
setp   hm2_7i76e.0.stepgen.02.maxaccel         [JOINT_2]STEPGEN_MAXACCEL
setp   hm2_7i76e.0.stepgen.02.maxvel           [JOINT_2]STEPGEN_MAXVEL

net j2-output     <= hm2_7i76e.0.stepgen.02.velocity-cmd
net j2-pos-fb     <= hm2_7i76e.0.stepgen.02.position-fb
net j2-enable     => hm2_7i76e.0.stepgen.02.enable

net j2-pos-cmd       <= joint.2.motor-pos-cmd
net j2-vel-cmd       <= joint.2.vel-cmd
net j2-pos-fb        => joint.2.motor-pos-fb
net j2-enable        <= joint.2.amp-enable-out

#*******************
#  JOINT 3
#*******************

setp   pid.j3.Pgain     [JOINT_3]P
setp   pid.j3.Igain     [JOINT_3]I
setp   pid.j3.Dgain     [JOINT_3]D
setp   pid.j3.bias      [JOINT_3]BIAS
setp   pid.j3.FF0       [JOINT_3]FF0
setp   pid.j3.FF1       [JOINT_3]FF1
setp   pid.j3.FF2       [JOINT_3]FF2
setp   pid.j3.deadband  [JOINT_3]DEADBAND
setp   pid.j3.maxoutput [JOINT_3]MAX_OUTPUT
setp   pid.j3.error-previous-target true
# This setting is to limit bogus stepgen
# velocity corrections caused by position
# feedback sample time jitter.
setp   pid.j3.maxerror 0.012700

net j3-index-enable  <=> pid.j3.index-enable
net j3-enable        =>  pid.j3.enable
net j3-pos-cmd       =>  pid.j3.command
net j3-pos-fb        =>  pid.j3.feedback
net j3-output        <=  pid.j3.output

# Step Gen signals/setup

setp   hm2_7i76e.0.stepgen.03.dirsetup        [JOINT_3]DIRSETUP
setp   hm2_7i76e.0.stepgen.03.dirhold         [JOINT_3]DIRHOLD
setp   hm2_7i76e.0.stepgen.03.steplen         [JOINT_3]STEPLEN
setp   hm2_7i76e.0.stepgen.03.stepspace       [JOINT_3]STEPSPACE
setp   hm2_7i76e.0.stepgen.03.position-scale  [JOINT_3]STEP_SCALE
setp   hm2_7i76e.0.stepgen.03.step_type        0
setp   hm2_7i76e.0.stepgen.03.control-type     1
setp   hm2_7i76e.0.stepgen.03.maxaccel         [JOINT_3]STEPGEN_MAXACCEL
setp   hm2_7i76e.0.stepgen.03.maxvel           [JOINT_3]STEPGEN_MAXVEL

net j3-output     <= hm2_7i76e.0.stepgen.03.velocity-cmd
net j3-pos-fb     <= hm2_7i76e.0.stepgen.03.position-fb
net j3-enable     => hm2_7i76e.0.stepgen.03.enable
net j3-pos-cmd    <= joint.3.motor-pos-cmd
net j3-vel-cmd    <= joint.3.vel-cmd
net j3-pos-fb     => joint.3.motor-pos-fb
net j3-enable     <= joint.3.amp-enable-out



#*******************
#  JOINT 4
#*******************

setp   pid.j4.Pgain     [JOINT_4]P
setp   pid.j4.Igain     [JOINT_4]I
setp   pid.j4.Dgain     [JOINT_4]D
setp   pid.j4.bias      [JOINT_4]BIAS
setp   pid.j4.FF0       [JOINT_4]FF0
setp   pid.j4.FF1       [JOINT_4]FF1
setp   pid.j4.FF2       [JOINT_4]FF2
setp   pid.j4.deadband  [JOINT_4]DEADBAND
setp   pid.j4.maxoutput [JOINT_4]MAX_OUTPUT
setp   pid.j4.error-previous-target true
# This setting is to limit bogus stepgen
# velocity corrections caused by position
# feedback sample time jitter.
setp   pid.j4.maxerror 0.012700

net j4-index-enable  <=> pid.j4.index-enable
net j4-enable        =>  pid.j4.enable
net j4-pos-cmd       =>  pid.j4.command
net j4-pos-fb        =>  pid.j4.feedback
net j4-output        <=  pid.j4.output

# Step Gen signals/setup

setp   hm2_7i76e.0.stepgen.04.dirsetup        [JOINT_4]DIRSETUP
setp   hm2_7i76e.0.stepgen.04.dirhold         [JOINT_4]DIRHOLD
setp   hm2_7i76e.0.stepgen.04.steplen         [JOINT_4]STEPLEN
setp   hm2_7i76e.0.stepgen.04.stepspace       [JOINT_4]STEPSPACE
setp   hm2_7i76e.0.stepgen.04.position-scale  [JOINT_4]STEP_SCALE
setp   hm2_7i76e.0.stepgen.04.step_type        0
setp   hm2_7i76e.0.stepgen.04.control-type     1
setp   hm2_7i76e.0.stepgen.04.maxaccel         [JOINT_4]STEPGEN_MAXACCEL
setp   hm2_7i76e.0.stepgen.04.maxvel           [JOINT_4]STEPGEN_MAXVEL

# ---closedloop stepper signals---

net j4-pos-cmd    <= joint.4.motor-pos-cmd
net j4-vel-cmd    <= joint.4.vel-cmd
net j4-output     <= hm2_7i76e.0.stepgen.04.velocity-cmd
net j4-pos-fb     <= hm2_7i76e.0.stepgen.04.position-fb
net j4-pos-fb     => joint.4.motor-pos-fb
net j4-enable     <= joint.4.amp-enable-out
net j4-enable     => hm2_7i76e.0.stepgen.04.enable

#*******************
#  SPINDLE
#*******************

setp   pid.s.Pgain     [SPINDLE_0]P
setp   pid.s.Igain     [SPINDLE_0]I
setp   pid.s.Dgain     [SPINDLE_0]D
setp   pid.s.bias      [SPINDLE_0]BIAS
setp   pid.s.FF0       [SPINDLE_0]FF0
setp   pid.s.FF1       [SPINDLE_0]FF1
setp   pid.s.FF2       [SPINDLE_0]FF2
setp   pid.s.deadband  [SPINDLE_0]DEADBAND
setp   pid.s.maxoutput [SPINDLE_0]MAX_OUTPUT
setp   pid.s.error-previous-target true

net spindle-index-enable  => pid.s.index-enable
net spindle-enable        =>  pid.s.enable
net spindle-vel-cmd-rpm-abs     => pid.s.command
net spindle-vel-fb-rpm-abs      => pid.s.feedback
net spindle-output        <=  pid.s.output

# ---digital potentionmeter output signals/setup---

setp   hm2_7i76e.0.7i76.0.0.spinout-minlim    [SPINDLE_0]OUTPUT_MIN_LIMIT
setp   hm2_7i76e.0.7i76.0.0.spinout-maxlim    [SPINDLE_0]OUTPUT_MAX_LIMIT
setp   hm2_7i76e.0.7i76.0.0.spinout-scalemax  [SPINDLE_0]OUTPUT_SCALE

net spindle-output      => hm2_7i76e.0.7i76.0.0.spinout
net spindle-enable      => hm2_7i76e.0.7i76.0.0.spinena
net spindle-ccw         => hm2_7i76e.0.7i76.0.0.spindir

# ---setup spindle control signals---

net spindle-vel-cmd-rps        <=  spindle.0.speed-out-rps
net spindle-vel-cmd-rps-abs    <=  spindle.0.speed-out-rps-abs
net spindle-vel-cmd-rpm        <=  spindle.0.speed-out
net spindle-vel-cmd-rpm-abs    <=  spindle.0.speed-out-abs
net spindle-enable             <=  spindle.0.on
net spindle-cw                 <=  spindle.0.forward
net spindle-ccw                <=  spindle.0.reverse
net spindle-brake              <=  spindle.0.brake
net spindle-revs               =>  spindle.0.revs
net spindle-at-speed           =>  spindle.0.at-speed
net spindle-vel-fb-rps         =>  spindle.0.speed-in
net spindle-index-enable      <=>  spindle.0.index-enable

# ---Setup spindle at speed signals---

sets spindle-at-speed true


#******************************
# connect miscellaneous signals
#******************************

#  ---HALUI signals---

net axis-select-x  halui.axis.x.select
net jog-x-pos      halui.axis.x.plus
net jog-x-neg      halui.axis.x.minus
net jog-x-analog   halui.axis.x.analog

net axis-select-y  halui.axis.y.select
net jog-y-pos      halui.axis.y.plus
net jog-y-neg      halui.axis.y.minus
net jog-y-analog   halui.axis.y.analog

net axis-select-z  halui.axis.z.select
net jog-z-pos      halui.axis.z.plus
net jog-z-neg      halui.axis.z.minus
net jog-z-analog   halui.axis.z.analog
net axis-select-a  halui.axis.a.select
net jog-a-pos      halui.axis.a.plus
net jog-a-neg      halui.axis.a.minus
net jog-a-analog   halui.axis.a.analog

net jog-selected-pos      halui.axis.selected.plus
net jog-selected-neg      halui.axis.selected.minus
net spindle-manual-cw     halui.spindle.0.forward
net spindle-manual-ccw    halui.spindle.0.reverse
net spindle-manual-stop   halui.spindle.0.stop
net machine-is-on         halui.machine.is-on
net jog-speed             halui.axis.jog-speed
net MDI-mode              halui.mode.is-mdi

#  ---coolant signals---

net coolant-mist      <=  iocontrol.0.coolant-mist
net coolant-flood     <=  iocontrol.0.coolant-flood

#  ---probe signal---

net probe-in     =>  motion.probe-input

#  ---motion control signals---

net in-position               <=  motion.in-position
net machine-is-enabled        <=  motion.motion-enabled

#  ---digital in / out signals---

#  ---estop signals---

net estop-out     <=  iocontrol.0.user-enable-out
net estop-out     =>  iocontrol.0.emc-enable-in

#  ---manual tool change signals---

loadusr -W hal_manualtoolchange
net tool-change-request     iocontrol.0.tool-change       =>  hal_manualtoolchange.change
net tool-change-confirmed   iocontrol.0.tool-changed      <=  hal_manualtoolchange.changed
net tool-number             iocontrol.0.tool-prep-number  =>  hal_manualtoolchange.number
net tool-prepare-loopback   iocontrol.0.tool-prepare      =>  iocontrol.0.tool-prepared

